#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running shm. Failure expected: Test aborted, timeout!"
./tst_sys_shm &

printf "%s\n" "-- Running shm test in $BAD_CONTAINER. Failure expected: Test aborted, timeout!"
# shellcheck disable=SC2154
podman exec "$BAD_CONTAINER" "$(get_path_to_script "$file")"
