#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running shm test in $cntr_orderly. Success expected: SHM access..."
podman exec "$cntr_orderly" ./tst_shm &

printf "%s\n" "-- Running shm test in $cntr_ipc_container. Success expected: SHM access..."
podman exec "$cntr_ipc_container" ./tst_shm
