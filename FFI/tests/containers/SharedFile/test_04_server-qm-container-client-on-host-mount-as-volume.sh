#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running server container in $BAD_CONTAINER"
# shellcheck disable=SC2086
podman exec -d $BAD_CONTAINER bash -c "podman run $CONTAINER_PARAMS --replace --name $cntr_mount_server $BASE_CONTAINER_IMAGE /var/tst_socket_fd --test-name $tst_name --file-name $f_name --mount --timeout-expected" > /dev/null

printf "%s\n" "-- Running client on host."
# shellcheck disable=SC2086
./tst_asilb_socket_fd --test-name $tst_name --file-name $f_name --mount --timeout-expected > /dev/null 2>&1
