#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running server container in $BAD_CONTAINER without mount file volume"
# shellcheck disable=SC2086
podman exec -d $BAD_CONTAINER bash -c "cd /var ; if [ -e $tst_name ]; then rm -f $tst_name; fi ; podman run $CONTAINER_PARAMS --replace --name $cntr_server $BASE_CONTAINER_IMAGE /var/tst_socket_fd --test-name $tst_name --file-name /var/$f_name" > /dev/null
sleep 3

printf "%s\n" "-- Running client container in $BAD_CONTAINER without mount file volume"
# shellcheck disable=SC2086
podman exec -d $BAD_CONTAINER bash -c "cd /var ; podman run $CONTAINER_PARAMS --replace --ipc container:$cntr_server --name $cntr_client $BASE_CONTAINER_IMAGE /var/tst_socket_fd --test-name $tst_name --file-name /var/$f_name" > /dev/null