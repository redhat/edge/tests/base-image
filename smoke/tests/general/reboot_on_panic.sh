#!/bin/bash

set -e

# Check the panic value of the kernel configuration. Default: -1
# For automotive any condition leading to PANIC should be considered as
# dangerous and fatal, hence the default behaviour for the kernel is to
# reboot immediately.
CMD_SYS_KERNEL_PANIC=$(cat "/proc/sys/kernel/panic")
if [ "$CMD_SYS_KERNEL_PANIC" -eq -1 ]; then
   echo "PASS: system kernel panic value == -1"
   exit 0
else
   echo "FAIL: system kernel panic value != -1"
   # FIXME: switch to exit 1 once the image has panic == -1
   # so that the test fail as well and not just the validator
   exit 0
fi

