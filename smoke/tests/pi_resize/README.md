# Smoke tests - Pi_resize

Smoke test for Pi_resize expanding the root partition to maximum size of the physical disk procedure.

## This Test Suite includes these tests

1. Check that pi_resize rpm is installed and its current version.

2. Check that root partition size ~= size of the physical disk.
