#!/bin/bash

# List of VFAT-related kernel modules
vfat_modules=("vfat" "fat")

# Flag to track if any module is loaded
vfat_loaded=0

echo "Checking if VFAT modules are disabled..."

for module in "${vfat_modules[@]}"; do

    # attempt loading the module first
    sudo modprobe "$module" 2>/dev/null

    if lsmod | grep -q "^$module"; then
        echo "FAIL: VFAT module '$module' is loaded."
        vfat_loaded=1
    fi
done

if [ "$vfat_loaded" -eq 0 ]; then
    echo "PASS: All VFAT modules are disabled."
    exit 0
else
    echo "FAIL: Some VFAT modules are still loaded."
    exit 1
fi
