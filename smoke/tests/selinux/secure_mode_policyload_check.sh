#!/bin/bash

set -uo pipefail

# check secure_mode_policyload status
if ! SECURE_MODE_STATUS=$(getsebool secure_mode_policyload 2>/dev/null); then
    echo "FAIL : The 'secure_mode_policyload' boolean is not recognized. SELinux may be disabled."
    exit 1
fi

if [[ $SECURE_MODE_STATUS == *"on"* ]]; then
    echo "INFO: secure_mode_policyload current is: ${SECURE_MODE_STATUS}"
    echo "PASS: secure_mode_policyload is enabled."
    exit 0
else
    echo "INFO: secure_mode_policyload current is: ${SECURE_MODE_STATUS}"
    echo "FAIL: secure_mode_policyload is disabled."
    exit 1
fi
